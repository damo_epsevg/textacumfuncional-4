package edu.upc.damo;

import android.app.Activity;
import android.content.res.Configuration;
import android.os.Bundle;
import android.text.Editable;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends Activity {

    EditText camp;
    TextView res;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        inicialitza();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        inicialitzaMenu(menu);
        return true;
    }

    private void inicialitza() {
        res =  findViewById(R.id.resultat);
    }

    private void esborraResultat() {
        res.setText("");
        camp.setText("");
    }

    private void inicialitzaMenu(Menu menu) {
        MenuItem menuItem = menu.findItem(R.id.action_afegeix);

        camp =  menuItem
                .getActionView()
                .findViewById(R.id.entradaDada);
        camp.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                return nouContingut(v, actionId, event);
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch (item.getItemId()) {
            case R.id.action_settings:
                return true;
            case R.id.action_esborra:
                esborraResultat();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }


    private void mostraAjut() {
    }




    private  abstract class Teclat {
        public final static int  DESCONEGUT = 0;
        public final static int  OK = 1;
        abstract public int accio( int actionId, KeyEvent event);
    }

    private  class TeclatHard extends Teclat{
        @Override
        public int accio(int actionId, KeyEvent event) {
            switch (event.getAction()) {
                case KeyEvent.ACTION_UP:
                    return OK;
            }
            return DESCONEGUT;
        }
    }
    private  class TeclatSoft extends Teclat{
        @Override
        public int accio(int actionId, KeyEvent event) {
            switch (actionId) {
                case EditorInfo.IME_ACTION_GO:
                    return OK;
            }
            return DESCONEGUT;
        }
    }

    private boolean hiHaTeclatHard() {
        return getResources().getConfiguration().keyboard != Configuration.KEYBOARD_NOKEYS;
    }


    private boolean nouContingut(TextView v, int actionId, KeyEvent event) {
        // Mirem si s'ha polsat un botó d'acció. En aquests casos l'event és null
        // (És el cas de teclat soft)

        Teclat t;

        if (hiHaTeclatHard())
             t = new TeclatHard();
        else
            t = new TeclatSoft();

        switch (t.accio(actionId, event)) {
            case Teclat.OK:
               afegeixAResultat(camp.getText());
               camp.setText("");
               amagaTeclat(v);
               return true;
        }

        return true;
    }


    private void amagaTeclat(View v) {
        InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(v.getWindowToken(), InputMethodManager.RESULT_UNCHANGED_SHOWN);
    }

    private void afegeixAResultat(Editable text) {
        res.append("\n");
        res.append(text);
    }
}


